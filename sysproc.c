#include "types.h"
#include "x86.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "pstat.h"

struct pstat s = {{0},{0},{0},{0}};

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}
//Modification 
int 
sys_settickets(int num)
{
  settickets(5);
  exit();
  return 0;
}

int
sys_getpinfo(struct pstat* t)
{
  struct pstat stat = {{0},{0},{0},{0}};
  getpinfo(&stat);
  int i;
  char *q = " ";
  for (i = 0 ; i < NPROC ; i++){
    if (stat.pid[i] == 0){
      q = "Not assigned";
    }
    else if(stat.queue[i] == 0){
      q = "High";
    }else{
      q = "Low";
    }
    cprintf("INUSE: %d ,PID: %d , HTICKS: %d ,LTICKS: %d, QUEUE: %s, NUMBER_OF_CHOSEN_TIMES: %d \n"
      , stat.inuse[i], stat.pid[i], stat.hticks[i], stat.lticks[i], q, stat.chosenTimes[i]);
  }
  exit();
  return 0;
}
//End of modification

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;
  
  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}
